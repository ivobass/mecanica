#ifndef FATURA_H
#define FATURA_H

// fatura.h
#include <string>
#include <ctime>

class Fatura {
 public:
  Fatura(std::string nif_cliente, double total_pago, time_t data);
  std::string nif_cliente() const;
  double total_pago() const;
  time_t data() const;

 private:
  std::string nif_cliente_;
  double total_pago_;
  time_t data_;
};
#endif // FATURA_H
