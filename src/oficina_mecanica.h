#ifndef OFICINA_MECANICA_H
#define OFICINA_MECANICA_H


// oficina_mecanica.h
#include <string>
#include <vector>
#include "cliente.h"
#include "fatura.h"
#include "compra.h"
#include "carro.h"

class OficinaMecanica {
 public:
  OficinaMecanica(std::string nome);
  void AdicionarCliente(const Cliente& cliente);
  void EmitirFatura(const Fatura& fatura);
  void RealizarCompra(const Compra& compra);
  void AdicionarCarro(const Carro& carro);
  void ListarPecasCompradas(std::string nome_ficheiro);
  std::string IdentificarClienteMaiorGasto();

 private:
  std::string nome_;
  std::vector<Cliente> clientes_;
  std::vector<Fatura> faturas_;
  std::vector<Compra> compras_;
  std::vector<Carro> carros_;
};


#endif // OFICINA_MECANICA_H
