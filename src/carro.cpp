// carro.cpp
#include "carro.h"

Carro::Carro(std::string marca, int ano_fabricacao, int cilindrada)
    : marca_(marca), ano_fabricacao_(ano_fabricacao), cilindrada_(cilindrada) {}

void Carro::AdicionarPeca(const Peca& peca) {
  pecas_.push_back(peca);
}

std::string Carro::marca() const {
  return marca_;
}

int Carro::ano_fabricacao() const {
  return ano_fabricacao_;
}

int Carro::cilindrada() const {
  return cilindrada_;
}

const std::vector<Peca>& Carro::pecas() const {
  return pecas_;
}

