
// oficina_mecanica.cpp
#include "oficina_mecanica.h"
#include <fstream>

OficinaMecanica::OficinaMecanica(std::string nome) : nome_(nome) {}

void OficinaMecanica::AdicionarCliente(const Cliente& cliente) {
  clientes_.push_back(cliente);
}

void OficinaMecanica::EmitirFatura(const Fatura& fatura) {
  faturas_.push_back(fatura);
}

// continuando oficina_mecanica.cpp
void OficinaMecanica::RealizarCompra(const Compra& compra) {
  compras_.push_back(compra);
}

void OficinaMecanica::AdicionarCarro(const Carro& carro) {
  carros_.push_back(carro);
}

void OficinaMecanica::ListarPecasCompradas(std::string nome_ficheiro) {
  std::ofstream ficheiro(nome_ficheiro);
  double total_gasto = 0;
  for (const Compra& compra : compras_) {
    ficheiro << compra.data() << " " << compra.descricao_peca().codigo() << " "
             << compra.descricao_peca().nome() << " " << compra.valor_pago()
             << std::endl;
    total_gasto += compra.valor_pago();
  }
  ficheiro << "Total gasto: " << total_gasto << std::endl;
  ficheiro.close();
}

std::string OficinaMecanica::IdentificarClienteMaiorGasto() {
  double maior_gasto = 0;
  std::string nif_cliente;
  for (const Cliente& cliente : clientes_) {
    if (cliente.total_gasto() > maior_gasto) {
      maior_gasto = cliente.total_gasto();
      nif_cliente = cliente.nif();
    }
  }
  return nif_cliente;
}
