// cliente.cpp
#include "cliente.h"

Cliente::Cliente(std::string cc, std::string nif, std::string telefone, std::string nome,
                 std::string morada, std::string marca_carro, std::string matricula)
    : cc_(cc),
      nif_(nif),
      telefone_(telefone),
      nome_(nome),
      morada_(morada),
      marca_carro_(marca_carro),
      matricula_(matricula),
      total_gasto_(0) {}

void Cliente::AdicionarGasto(double gasto) {
  total_gasto_ += gasto;
}

std::string Cliente::cc() const {
  return cc_;
}

std::string Cliente::nif() const {
  return nif_;
}

std::string Cliente::telefone() const {
  return telefone_;
}

std::string Cliente::nome() const {
  return nome_;
}

std::string Cliente::morada() const {
  return morada_;
}

std::string Cliente::marca_carro() const {
  return marca_carro_;
}

std::string Cliente::matricula() const {
  return matricula_;
}

double Cliente::total_gasto() const {
  return total_gasto_;
}


