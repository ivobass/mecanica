#ifndef CARRO_H
#define CARRO_H


// carro.h
#include <string>
#include <vector>
#include "peca.h"

class Carro {
 public:
  Carro(std::string marca, int ano_fabricacao, int cilindrada);
  void AdicionarPeca(const Peca& peca);
  std::string marca() const;
  int ano_fabricacao() const;
  int cilindrada() const;
  const std::vector<Peca>& pecas() const;

 private:
  std::string marca_;
  int ano_fabricacao_;
  int cilindrada_;
  std::vector<Peca> pecas_;
};

#endif // CARRO_H
