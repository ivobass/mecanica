#ifndef PECA_H
#define PECA_H


// peca.h
#include <string>

class Peca {
 public:
  Peca(std::string codigo, std::string nome);
  std::string codigo() const;
  std::string nome() const;

 private:
  std::string codigo_;
  std::string nome_;
};

#endif // PECA_H
