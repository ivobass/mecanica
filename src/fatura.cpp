// fatura.cpp
#include "fatura.h"

Fatura::Fatura(std::string nif_cliente, double total_pago, time_t data)
    : nif_cliente_(nif_cliente), total_pago_(total_pago), data_(data) {}

std::string Fatura::nif_cliente() const {
  return nif_cliente_;
}

double Fatura::total_pago() const {
  return total_pago_;
}

time_t Fatura::data() const {
  return data_;
}
