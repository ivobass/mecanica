#ifndef CLIENTE_H
#define CLIENTE_H
// cliente.h
#include <string>

class Cliente {
 public:
  Cliente(std::string cc, std::string nif, std::string telefone, std::string nome,
          std::string morada, std::string marca_carro, std::string matricula);
  void AdicionarGasto(double gasto);
  std::string cc() const;
  std::string nif() const;
  std::string telefone() const;
  std::string nome() const;
  std::string morada() const;
  std::string marca_carro() const;
  std::string matricula() const;
  double total_gasto() const;

 private:
  std::string cc_;
  std::string nif_;
  std::string telefone_;
  std::string nome_;
  std::string morada_;
  std::string marca_carro_;
  std::string matricula_;
  double total_gasto_;
};


#endif // CLIENTE_H
