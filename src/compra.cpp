
// compra.cpp
#include "compra.h"

Compra::Compra(Peca descricao_peca, double valor_pago, time_t data)
    : descricao_peca_(descricao_peca), valor_pago_(valor_pago), data_(data) {}

Peca Compra::descricao_peca() const {
  return descricao_peca_;
}

double Compra::valor_pago() const {
  return valor_pago_;
}

time_t Compra::data() const {
  return data_;
}
