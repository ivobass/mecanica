#ifndef COMPRA_H
#define COMPRA_H

// compra.h
#include "peca.h"
#include <ctime>

class Compra {
 public:
  Compra(Peca descricao_peca, double valor_pago, time_t data);
  Peca descricao_peca() const;
  double valor_pago() const;
  time_t data() const;

 private:
  Peca descricao_peca_;
  double valor_pago_;
  time_t data_;
};

#endif // COMPRA_H
