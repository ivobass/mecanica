// peca.cpp
#include "peca.h"

Peca::Peca(std::string codigo, std::string nome) : codigo_(codigo), nome_(nome) {}

std::string Peca::codigo() const {
  return codigo_;
}

std::string Peca::nome() const {
  return nome_;
}
